import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
  } from "react-native";
  
  
  export default function Menu({navigation}) {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate("CountMoviment")}
        >
          <Text>COUNT</Text>
        </TouchableOpacity>
  
        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate("ImageMoviment")}
          >
          <Text>IMAGE</Text>
        </TouchableOpacity>
     
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    menu: {
      padding: 10,
      margin: 5,
      backgroundColor: "pink",
      borderRadius: 5,
    },
  });
  