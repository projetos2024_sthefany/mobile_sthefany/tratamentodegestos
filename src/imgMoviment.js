import React, { useRef, useState, useEffect } from "react";
import { View, Dimensions, PanResponder, Button, Image, StyleSheet, Text } from "react-native";

const ImageMoviment = () => {
  const screenWidth = Dimensions.get("window").width;
  const [direction, setDirection] = useState(0);
  const images = [
    "https://via.placeholder.com/150/FF5733/FFFFFF",
    "https://via.placeholder.com/150/33FF57/FFFFFF",
    "https://via.placeholder.com/150/5733FF/FFFFFF",  
  ];

  2
  const panResponder = useRef(
    PanResponder.create({
        onStartShouldSetPanResponder:()=>true,
        onPanResponderRelease:(event, gestureState)=>{
          if (gestureState.dx > 50) {
            setDirection((prevDirection) => {
              if (prevDirection === images.length - 1) {
                return 0;
          } else { 
            return prevDirection + 1;
          
            }
          }
      )}
      },
    })
  ).current;

  

  return(
     <View style={styles.container} {...panResponder.panHandlers} >
      <Image source={{ uri: images[direction] }} style={styles.image}/>
     </View>
  )
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#fff'
  },
  image:{
    width: 300,
    height: 300,
    resizeMode: 'cover',
  },
});
export default ImageMoviment;
